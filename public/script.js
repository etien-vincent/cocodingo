const gameArea = document.getElementById("game-area");

let gameState = [];
let selectedPileId;
let currentPlayer = false;
let flipMode = true;
let lockedPileId;

for (let index = 1; index < 10; index++) {
	if (Math.floor(Math.random() * 2) == 0) {
		gameState.push([{ color: "black", value: index }]);
	} else {
		gameState.push([{ color: "white", value: 10 - index }]);
	}
	if (Math.floor(Math.random() * 2) == 0) {
		gameState.push([{ color: "white", value: index }]);
	} else {
		gameState.push([{ color: "black", value: 10 - index }]);
	}
}

function drawGame() {
	gameArea.innerHTML = "";
	for (let i = 0; i < gameState.length; i++) {
		const topCard = gameState[i][0];
		const cardElement = document.createElement("div");
		cardElement.classList.add("card");
		cardElement.classList.add(topCard.color);
		if (topCard.owner) {
			cardElement.classList.add("red-owned");
		} else if (topCard.owner === false) {
			cardElement.classList.add("blue-owned");
		}
		if (i === lockedPileId) {
			cardElement.classList.add("locked");
		}
		cardElement.innerHTML = `<span class="value">${topCard.value}</span>${
			gameState[i].length > 1
				? `<span class="pile-size">(${gameState[i].length}) </span>`
				: ""
		}`;
		cardElement.id = "pile-" + i;
		cardElement.onclick = () => handleClick(i);
		gameArea.appendChild(cardElement);
	}
}

function handleClick(pileId) {
	if (flipMode) {
		flipPile(pileId);
	} else {
		if (selectedPileId !== undefined) {
			const selected = selectedPileId;
			document
				.getElementById("pile-" + selectedPileId)
				.classList.remove("selected");
			selectedPileId = undefined;
			fusePiles(selected, pileId);
		} else {
			selectedPileId = pileId;
			document.getElementById("pile-" + pileId).classList.add("selected");
		}
	}
}

function fusePiles(topId, bottomId) {
	if (topId !== bottomId) {
		if (gameState[topId].length === 1) {
			if (
				gameState[bottomId][0].owner === currentPlayer ||
				gameState[bottomId][0].owner === undefined
			) {
				if (
					gameState[topId][0].value > gameState[bottomId][0].value ||
					gameState[bottomId][0].color !== gameState[topId][0].color
				) {
					gameState[bottomId].unshift(gameState[topId][0]);
					gameState[bottomId][0].owner = currentPlayer;
					gameState.splice(topId, 1);

					changeMode();
				}
			} else {
				if (
					gameState[topId][0].value > gameState[bottomId][0].value &&
					gameState[bottomId][0].color !== gameState[topId][0].color
				) {
					let newPile = [];
					newPile.push(gameState[topId][0]);
					newPile.push(gameState[bottomId][0]);
					for (let i = 1; i < gameState[bottomId].length; i++) {
						const newLength = gameState.push([
							gameState[bottomId][i],
						]);
						gameState[newLength - 1][0].owner = undefined;
					}
					gameState.splice(topId, 1);
					gameState[bottomId] = newPile;
					gameState[bottomId][0].owner = currentPlayer;
					changeMode();
				}
			}
		}
	}
	drawGame();
}

function changePlayer() {
	currentPlayer = !currentPlayer;
	if (currentPlayer) {
		document.getElementById("change-player").classList.add("red");
		document.getElementById("change-player").classList.remove("blue");
		document.getElementById("current-player-info").innerHTML =
			"Red plays : ";
	} else {
		document.getElementById("change-player").classList.remove("red");
		document.getElementById("change-player").classList.add("blue");
		document.getElementById("current-player-info").innerHTML =
			"Blue plays : ";
	}
}

function flipPile(pileId) {
	if (gameState[pileId].length === 1 && pileId !== lockedPileId) {
		let newColor;
		if (gameState[pileId][0].color === "black") {
			newColor = "white";
		} else {
			newColor = "black";
		}
		gameState[pileId][0].color = newColor;
		gameState[pileId][0].value = 10 - gameState[pileId][0].value;
		lockedPileId = pileId;
		changePlayer();
		changeMode();
		drawGame();
	}
}

function changeMode() {
	flipMode = !flipMode;
	if (flipMode) {
		document.getElementById("current-mode-info").innerHTML =
			"Flip and lock until your next turn";
	} else {
		document.getElementById("current-mode-info").innerHTML =
			"Build a tower or attack";
	}
}

drawGame();
